-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Apr 2019 pada 06.11
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `disc`
--
--
-- Struktur dari tabel `kategori`
--
--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `kode`, `nama`) VALUES
(1, '01', 'Celana');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lini`
--
--
-- Dumping data untuk tabel `lini`
--

INSERT INTO `lini` (`id`, `kode`, `lini_product`) VALUES
(1, '01', 'Panjang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--
--
-- Dumping data untuk tabel `migrations`
--
-- --------------------------------------------------------

--
-- Struktur dari tabel `toko`
--
--
-- Dumping data untuk tabel `toko`
--

INSERT INTO `toko` (`id`, `kode`, `nama`) VALUES
(1, '01', 'Amigo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `discount`
--
--
-- Dumping data untuk tabel `discount`
--

INSERT INTO `discount` (`id`, `kodedisc`, `nama`, `kategori_id`, `periode_awal`, `periode_akhir`, `besaran_disc`, `lini_id`, `toko_id`, `gambar_brosur`) VALUES
(1, '00001', 'Joger', 1, '2019-04-12 11:01:43', '2019-08-12 11:01:50', 0.50, 1, 1, 'Gratis Minum');

-- --------------------------------------------------------

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
