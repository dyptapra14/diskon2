<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lini', function (Blueprint $table) {
            $table->increments('id');
            $table->char('kode', 4)->unique();
            $table->string('lini_product', 256);
            //$table->timestamps();
        });

        Schema::table('discount', function (Blueprint $table) {
            $table->foreign('lini_id')->references('id')->on('lini');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lini');
    }
}
