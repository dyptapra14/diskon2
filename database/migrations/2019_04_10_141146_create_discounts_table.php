<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount', function (Blueprint $table) {
            $table->increments('id');
            $table->char('kodedisc', 5)->unique();
            $table->string('nama', 256);
            $table->integer('kategori_id')->unsigned();
            $table->datetime('periode_awal');
            $table->datetime('periode_akhir');
            $table->float('besaran_disc', 5, 2);
            $table->integer('lini_id')->unsigned();;
            $table->integer('toko_id')->unsigned();;
            $table->text('gambar_brosur'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount');
    }
}
