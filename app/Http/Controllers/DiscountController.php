<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\discount;
use Illuminate\Support\Facades\DB;

class DiscountController extends Controller
{
    public function getAllDisc(){
        //return Discount::all();
        return DB::table('discount')->select('discount.id', 'discount.kodedisc', 'discount.nama', 'kategori.kode as kategori_kode','kategori.nama as kategori_nama', 'discount.periode_awal', 'discount.periode_akhir', 'discount.besaran_disc', 'lini.kode as lini_kode', 'lini.lini_product as lini_product', 'toko.kode as kode_toko', 'toko.nama as toko_nama', 'discount.gambar_brosur')->join('kategori', 'discount.kategori_id', '=', 'kategori.id')->join('lini', 'discount.lini_id', 'lini.id')->join('toko', 'discount.toko_id', 'toko.id')->get();
    }

    public function addDisc(request $request){
        $discount = new Discount;
        $discount->kodedisc = $request->kodedisc;
        $discount->nama = $request->nama;
        $discount->kategori_id = $request->kategori_id;
        $str = str_replace('.000Z','', $request->periode_awal);
        $str2 = str_replace('.000Z','', $request->periode_akhir);
        $discount->periode_awal = str_replace('T', ' ', $str);
        $discount->periode_akhir = str_replace('T', ' ', $str2);
        $discount->besaran_disc = $request->besaran_disc;
        $discount->lini_id = $request->lini_id;
        $discount->toko_id = $request->toko_id;
        $discount->gambar_brosur = $request->gambar_brosur;
        $discount->save();

        return Discount::all();
    }

    public function updateDisc(request $request, $id){
        $kodedisc = $request->kodedisc;
        $nama = $request->nama;
        $kategori_id = $request->kategori_id;
        $str = str_replace('.000Z','', $request->periode_awal);
        $str2 = str_replace('.000Z','', $request->periode_akhir);
        $periode_awal = str_replace('T', ' ', $str);;
        $periode_akhir = str_replace('T', ' ', $str2);
        $besaran_disc = $request->besaran_disc;
        $lini_id = $request->lini_id;
        $toko_id = $request->toko_id;
        $gambar_brosur = $request->gambar_brosur;

        $discount = Discount::find($id);
        $discount->kodedisc = $kodedisc;
        $discount->nama = $nama;
        $discount->kategori_id = $kategori_id;
        $discount->periode_awal = $periode_awal;
        $discount->periode_akhir = $periode_akhir;
        $discount->besaran_disc = $besaran_disc;
        $discount->lini_id = $lini_id;
        $discount->toko_id = $toko_id;
        $discount->gambar_brosur = $gambar_brosur;
        $discount->save();

        return Discount::all();
    }

    public function delDisc(request $request, $id){
        $discount = Discount::find($id);
        $discount->delete();

        return Discount::all();
    }
}
