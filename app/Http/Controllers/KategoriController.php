<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori;

class KategoriController extends Controller
{
    public function index(){
        return Kategori::all();
    }

    public function addKategori(request $request){
        $kategori = new Kategori;
        $kategori->kode = $request->kode;
        $kategori->nama = $request->nama;
        $kategori->save();

        return Kategori::all();
    }

    public function updateKategori(request $request, $id){
        $kode = $request->kode;
        $nama = $request->nama;

        $kategori = Kategori::find($id);
        $kategori->kode = $kode;
        $kategori->nama = $nama;
        $kategori->save();
        return Kategori::all();
    }

    public function delKategori($id){
        $kategori = Kategori::find($id);
        $kategori->delete();
        return Kategori::all();
    }
}
