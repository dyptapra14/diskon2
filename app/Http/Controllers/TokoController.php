<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\toko;

class TokoController extends Controller
{
    public function index(){
        return Toko::all();
    }

    public function addToko(request $request){
        $toko = new Toko;
        $toko->kode = $request->kode;
        $toko->nama = $request->nama;
        $toko->save();

        return Toko::all();
    }

    public function updateToko(request $request, $id){
        $kode = $request->kode;
        $nama = $request->nama;

        $toko = Toko::find($id);
        $toko->kode = $kode;
        $toko->nama = $nama;
        $toko->save();
        return Toko::all();
    }

    public function delToko($id){
        $toko = Toko::find($id);
        $toko->delete();
        return Toko::all();
    }
}
