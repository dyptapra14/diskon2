<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\discount;
use Carbon;

class getDisc extends Controller
{
    public function join(){
        
    }
    public function getActiveDisc(){
        $time = Carbon\Carbon::now();
        return DB::table('discount')->select('discount.kodedisc', 'discount.nama', 'kategori.kode as kategori_kode','kategori.nama as kategori_nama', 'discount.periode_awal', 'discount.periode_akhir', 'discount.besaran_disc', 'lini.kode as lini_kode', 'lini.lini_product as lini_product', 'toko.kode as kode_toko', 'toko.nama as nama_toko', 'discount.gambar_brosur')->join('kategori', 'discount.kategori_id', '=', 'kategori.id')->join('lini', 'discount.lini_id', 'lini.id')->join('toko', 'discount.toko_id', 'toko.id')->where('discount.periode_akhir','>=', $time->toDateString())->get();
    }

    public function getInfoDisc($kodedisc){
        if(Discount::find($kodedisc)){
            $discount = Discount::find($kodedisc);
        }else{
            $discount = DB::table('discount')->addSelect(DB::raw("'Tidak ditemukan' as message"))->get();
        }
        return $discount;
    }
}
