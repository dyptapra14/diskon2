<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lini;

class LiniController extends Controller
{
    public function index(){
        return Lini::all();
    }

    public function addLini(request $request){
        $lini = new Lini;
        $lini->kode = $request->kode;
        $lini->lini_product = $request->lini_product;
        $lini->save();

        return Lini::all();
    }

    public function updateLini(request $request, $id){
        $kode = $request->kode;
        $lini_product = $request->lini_product;

        $lini = Lini::find($id);
        $lini->kode = $kode;
        $lini->lini_product = $lini_product;
        $lini->save();
        return Lini::all();
    }

    public function delLini($id){
        $lini = Lini::find($id);
        $lini->delete();
        return Lini::all();
    }
}
