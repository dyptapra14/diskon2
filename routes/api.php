<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('kategori', 'KategoriController@index');
Route::get('discount', 'DiscountController@getAllDisc');
Route::get('lini', 'LiniController@index');
Route::get('toko', 'TokoController@index');
Route::get('view', 'ViewController@index');
Route::get('getActiveDisc', 'getDisc@getActiveDisc');
Route::get('getInfoDisc/{kodedisc}', 'getDisc@getInfoDisc');

Route::post('kategori', 'KategoriController@addKategori');
Route::post('discount', 'DiscountController@addDisc');
Route::post('lini', 'LiniController@addLini');
Route::post('toko', 'TokoController@addToko');
//
Route::put('kategori/{id}', 'KategoriController@updateKategori');
Route::put('discount/{kodedisc}', 'DiscountController@updateDisc');
Route::put('lini/{id}', 'LiniController@updateLini');
Route::put('toko/{id}', 'TokoController@updateToko');
//
Route::delete('kategori/{id}', 'KategoriController@delKategori');
Route::delete('discount/{kodedisc}', 'DiscountController@delDisc');
Route::delete('lini/{id}', 'LiniController@delLini');
Route::delete('toko/{id}', 'TokoController@delToko');