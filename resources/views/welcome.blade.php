<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Setup Discount</title>

        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> --}}

    </head>
    <body>
        <div id="app">
            <navb></navb>
            <div class="container">
                <cons></cons>
                {{-- <lays></lays> --}}

            </div>

        </div>
        <script src="{{ asset('js/app.js') }}"></script>

    </body>
</html>
